import React, { Component } from 'react'
import auth0 from 'auth0-js'
import { API_URL } from '../../constants'

import logomarca from '../../assets/images/logo-ioasys.png'

import './login.scss'

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
          authorized: '',
          error: '',

          token: '',
          client: '',
          uid: '',

          email: '',
          password: '',
        };
    }
   

    login = event => {

        let header;
        event.preventDefault();

        var options = {
            method: 'POST',
            headers: {
              'content-type' : 'application/json',
              'access-control-expose-headers' : 'access-token, expiry, token-type, uid, client, resource-type'
            },
            body : JSON.stringify({
                email: this.state.email,  
                password: this.state.password,  
            }),
            mode: 'cors',
            cache: 'default'
          };   

        fetch(API_URL, options)
        .then(response => {

            this.setState({
                authorized: response.statusText,
                token: response.headers.get('access-token'),
                client: response.headers.get('client'),
                uid: response.headers.get('uid'),
            });

            {this.state.authorized != "Unauthorized"
                ? this.props.history.push('/dashboard', 
                    { 
                        uid: response.headers.get('uid'),
                        token: response.headers.get('access-token'),
                        client: response.headers.get('client') 
                    }) 

                :  this.setState({error: "Erro ao logar, verifique os dados inseridos e tente novamente!"});
            }             
        })
        .catch(error => {
            this.setState({error: "Erro ao logar, verifique os dados inseridos e tente novamente!"});
        })
    };

    render() {

        return (
            <section>
                <div id="login">
                    <div class="container">

                        <div class="row">
                            <div class="col-xs-12 col-md-12 login-area"> 
                                <div>
                                    <img src={logomarca} class="logomarca"/>
                                    <h1>Bem vindo as empresas</h1>
                                    <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>

                                    <form onSubmit={this.login}>

                                        <div class="form-fields">
                                            <input 
                                            type="email" 
                                            name="email" 
                                            placeholder="E-mail" 
                                            required
                                            onChange={event => { 
                                                this.setState({ email: event.target.value }) }}/> 

                                            <input 
                                            type="password" 
                                            name="password" 
                                            placeholder="Senha" 
                                            required
                                            onChange={event => { 
                                                this.setState({ password: event.target.value }) }} /> 
                                        </div>

                                        <button class="submit" type="submit">Entrar</button>

                                    </form>
                               
                                    {
                                        this.state.error != '' &&   
                                        <p class="error">{this.state.error}</p>
                                    }   

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        )
    }
}
