import React, { Component } from "react"

import './enterprises.scss'


export const Enterprise = ({photo, name, description}) => {
   
    return (

        <section id="enterprise">
            <div class="container">
                <div class="enterprise-single">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <h2>{name}</h2>
                            <img src={photo} alt="Enterprise photo" class="photo"/>
                            <p>{description}</p>       
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

