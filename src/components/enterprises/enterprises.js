import React, { Component } from 'react'
import { Enterprise } from './enterprise'

import './enterprises.scss'
import companyPhoto from '../../assets/images/company.jpg'

export default class Enterprises extends Component {

    constructor(props) {
        super(props);
        this.state = {
            photo: '',
            name: '',
            description: '',
            enterprise: false
        }
    }

    openEnterprise(photo, name, description){   
        this.setState({
            photo: photo,
            name: name,
            description: description,
            enterprise: true
        }) 
    }

    render(){

        const {enterprises} = this.props;

        return(

            <div>   
                <section id="enterprises">
                    <div class="container">
                        {  
                            enterprises.map((enterprises, index)=>{
                                return(
                                    <div class="enterprises-result"  onClick={()=> this.openEnterprise(companyPhoto, enterprises.enterprise_name, enterprises.description)} >
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4 col-lg-4">
                                                <img src={companyPhoto} class="photo" alt="Enterprise photo"/>
                                            </div>

                                            <div class="col-xs-12 col-md-4 col-lg-8">
                                                <h2>        
                                                    {enterprises.enterprise_name}  
                                                </h2>
                                                <p class="type">{enterprises.enterprise_type.enterprise_type_name}</p>
                                                <p class="country">{enterprises.country}</p>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }  
                    </div>
                </section>

                {
                    this.state.enterprise  &&       
                        <Enterprise 
                            photo={this.state.photo}
                            name={this.state.name}
                            description={this.state.description}
                        />  
                } 

            </div>
            
        )
    }
}

    