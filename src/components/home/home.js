import React, { Component } from 'react'
import MaterialIcon, {colorPalette} from 'material-icons-react'
import { enterprise_search_URL } from '../../constants'
import Enterprises from '../enterprises/enterprises'

import logomarca from '../../assets/images/white-logo-ioasys.png'
import './home.scss'

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: true,

            token: '',
            client: '',
            uid: '',

            searchTerm: '',
            enterprises: [],
            hideSection: false,
            error: '',
        };

        this.searchCompany = this.searchCompany.bind(this);
    }

    componentDidMount = () => {
        this.setState({
            uid: this.props.location.state.uid,
            token: this.props.location.state.token,
            client: this.props.location.state.client
        });
    }

    searchCompany = (event) => {

        event.preventDefault();

        //se input de pesquisa estiver vazio, execute o evento toggle
        if(!this.state.searchTerm){
            this.setState(prevState => ({
                isToggleOn: !prevState.isToggleOn
            }));  
        }

        //se estiver preenchido, inicia a busca
        else{
            const URL = enterprise_search_URL + this.state.searchTerm;
            var options = {
                method: 'GET',
                headers: {
                    'content-type' : "application/json",
                    'access-token' : this.state.token,
                    'client' : this.state.client,
                    'uid' : this.state.uid
                },
                mode: 'cors',
                cache: 'default'
            };

            fetch(URL, options)
            .then(response => response.json()) 
            .then(json => {
                const enterprises = json.enterprises;
                
                { enterprises == ""
                   ? this.setState({error: "Nenhuma empresa encontrada, tente buscar novamente!", enterprises: ''})

                   : this.setState({ enterprises, hideSection : true, error: '' })
                }
                  
            })
            .catch(error => {
                this.setState({error: "Nenhuma empresa encontrada, tente buscar novamente!"})
            })
        }    
    }

    render() {

        return (
            <div>

                <section id="search">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-12"> 

                                <form> 
                                    <div class="form-field">
                                        
                                        {this.state.isToggleOn 
                                            ?  <img src={logomarca} class="logomarca"/>
                                            :  <input 
                                                    type="text" 
                                                    name="company" 
                                                    placeholder="Pesquisar" 
                                                    class="field"
                                                    onChange={event => { 
                                                        this.setState({ searchTerm: event.target.value }) }}/>
                                        }

                                        <MaterialIcon 
                                            onClick= {this.searchCompany}
                                            icon="search" 
                                            size={30} 
                                            color='#ffffff'
                                        />
                                    </div>  
                                </form>

                            </div>
                        </div>
                    </div>
                </section>

                {
                    this.state.error != '' &&   
                    <div> <p class="error">{this.state.error}</p> </div>
                }   

                <section id="home" hidden = {this.state.hideSection}>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-12"> 
                                <h2>Clique na busca para iniciar.</h2>
                            </div>
                        </div>
                    </div>
                </section>
 
                {
                    this.state.enterprises != '' &&       
                        <Enterprises enterprises={this.state.enterprises} />    
                } 

            </div>
        )
    }
}
